## this function returns the gabor feature for an image
## parameter
##	img: 2-D dimension aligned gray image
## result
##	gabor: 1-D array of gabor features in 8 orientations and 5 scales

import numpy as np
from skimage.filter import gabor_filter

def gabor_magnitude(img):
	data = []
	for theta in range(4):
		theta = theta /4. *np.pi  # 8 orientations
		#real, im = gabor_filter(img, frequency, theta)
		#mag = (real**2 + im**2)** 0.5
		#data.append(mag.flatten())
		for frequency in (0.2, 0.3, 0.4 ):  # 3 scales
			real, im = gabor_filter(img, frequency, theta)
			mag = (real**2 + im**2)** 0.5
			data.append(mag.flatten())

	# convert list to matrix
	data = np.array(data)
	# arrange the data so that magnitude of each pixel become neighbor dimensions
	# flatten matrix
	return data.flatten()
	#return data
