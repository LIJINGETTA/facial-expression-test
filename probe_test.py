## this function recognize a facial expression out of 6 expressions (anger, disgust, fear, joy, sad, surprise) for an image

## expression, scores = test( probe, landmarkfile)
## parameters:
##	probe: path to the image file, e.g './test.png'
##	landmarkfile: path to the corresponding landmarks file
## results:
##	expression (int: 0-5): the predicted expression. 0: anger, 1: disgust, 2: fear, 3:joy, 4: sad, 5: surprise
##	scores (array of 6 values): the probabilistic values for each expression. s.t. sum of the scores are equal to 1  

from face_alignment import CropFace
from scikit_lbp import region_hist
from sci_Gabor import gabor_magnitude
from sklearn.externals import joblib
from skimage.feature import local_binary_pattern

import numpy as np
import Image
import time
import cv2

def test( probe, landmarkfile):
	# face_alignment

	x = [] # x coordinates of landmarks
        y = [] # y coordinates of landmarks

        with open(landmarkfile,'r') as myfile:
            count = 0
            for line in myfile:
                if count>1 and len(line)>2:
                    fields = line.rstrip('\n').split('\t')
                    y.append(fields[0])
                    x.append(fields[1])
                    
                count = count +1

        x = map(float, x)
        y = map(float, y)
	lm = np.append(x[16:],y[16:]) 
        # get landmarks of eye centers
        #left eye  [36:42);  right_eye: [42: 48)
        right_x, right_y = np.mean(x[42:48]), np.mean(y[42:48])
        left_x, left_y = np.mean(x[36:42]), np.mean(y[36:42])
        # read images and alignment
        image = Image.open(probe)  
	aligned_file =  './processing/' + probe.split('/')[-1]   
        CropFace(image, eye_left=(left_y,left_x), eye_right=(right_y, right_x), offset_pct=(0.26,0.3), dest_sz=(120,140)).save(aligned_file)
	
	## extract features
	# lbp
	color = cv2.imread(aligned_file)
	image = cv2.cvtColor(color, cv2.COLOR_BGR2GRAY)
	lbp = local_binary_pattern( image, P= 8, R=2, method='nri_uniform')
	lbp = region_hist(lbp, 7, 6)
	
	# gabor
	image = image[slice(0,None,4),slice(0,None,4)]
	gabor = gabor_magnitude(image)
	pca = joblib.load('./gabor_pca.pkl')
	gabor = pca.transform(gabor)

	# landmarks

	## classification
	lbp_clf = joblib.load('./classifiers/BU-CK_lbp_SVM.pkl')
	lbp_scores = lbp_clf.decision_function(lbp)
	gabor_clf = joblib.load('./classifiers/BU-CK_gabor_SVM.pkl')
	gabor_scores = gabor_clf.decision_function(gabor)
	lm_clf = joblib.load('./classifiers/BU-CK_lm_SVM.pkl')
	lm_scores = lm_clf.decision_function(lm)
	score = lbp_scores + gabor_scores + lm_scores
	score = score/3
	y_pred = np.where(score ==score.argmax() )
	
	#y_pred = lbp_clf.predict(lbp)
	
	return y_pred, score

