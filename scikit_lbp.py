## this function returns LBP features of an image
## parameters:
##	lbp: 2-D matrix of lbp map for an aligned gray image
##	x_number: number of regions in vertical direction
##	y_number: number of regions in horizontal direction
## result: lbp features of the lbp map, size 7*6
 
import numpy as np
from skimage.feature import local_binary_pattern

def region_hist(lbp, x_number = 7, y_number = 6):
	x_range, y_range = lbp.shape
	x_step, y_step = int(x_range/x_number), int(y_range/y_number)
    
    # compute histogram for each region
	feature = []
	for x in range(x_number):
 		for y in range(y_number):
			temp = lbp[x*x_step: (x+1)*x_step, y*y_step: (y+1)*y_step]
			hist, bins = np.histogram(temp.flatten(),59,[0,59])
			feature.append(hist)
            
	feature = np.array(feature)
	return feature.flatten()
