This folder is part of the facial expression recognition demo project.

1. These folder include files and subfolders as follows:
face expression test\
					face_alignment.py  -- preprocess the image
					sci_Gabor.py  -- extract gabor feature
					scikit_lbp.py  -- extract lbp feature
					face_fit.so  -- so that the face_fit lib can be used
					ExpressionClassifier.py -- a class of classification method
					classifiers\   --  classifiers to recognize expression
							BU-CK_part_lbp_SVM.pkl
							BU-CK_part_gabor_SVM.pkl
							BU-CK_lm_SVM.pkl
					test\ -- test images
							img1.jpg
							....
					processing\ -- store aligned faces (mediate data)
					
2. Libraries
To run the code, please make sure that following libraries should be included for python:
	opencv
	skimage
	sklearn
	face-analysis-sdk		
	
3. How to use it

run python code as follows:

	import ExpressionClassifier
	classifier = ExpressionClassifier()
	decision, prob = classifier.run(probe)
 
function description
 	decision, prob= classifier.run(probe)

probe: path to the test image, e.g.: './test/test.jpg'

decision: an integer which representing the recognized expression 
		range[0,5]
		0-anger, 1-disgust, 2-fear, 3-joy, 4-sad, 5-surprise

prob: 1-D array of float at the size of 6, indicating the probabilities of the probe being 6 expressions, in the order of 'anger, disgust, fear, joy, sad, surprise'




Remarks: 
For the time being, just use the lbp feature which performs best. Minor changes may be added to the classification based gabor and landmarks.

Time cost: about 0.8s for one image (including landmarking)


