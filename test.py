from face_alignment import CropFace
from scikit_lbp import region_hist
from sci_Gabor import gabor_magnitude
from sklearn.externals import joblib
from skimage.feature import local_binary_pattern
from skimage.filter import gabor_filter
from face_fit import FaceLandmarkDetector

import numpy as np
import Image
import cv2


def test(probe):
	#get landmarks
	color = cv2.imread(probe)
	gray = cv2.cvtColor(color, cv2.COLOR_BGR2GRAY)
	ld = FaceLandmarkDetector()
	landmarks = ld.get_face_landmarks(gray)
	if landmarks ==None:
		return 7,0
	x = landmarks[:,1]
	y = landmarks[:,0]
	# face_alignment

	lm = np.append(x[16:],y[16:]) 
        # get landmarks of eye centers
        #left eye  [36:42);  right_eye: [42: 48)
        right_x, right_y = np.mean(x[42:48]), np.mean(y[42:48])
        left_x, left_y = np.mean(x[36:42]), np.mean(y[36:42])
        # read images and alignment
        image = Image.open(probe)  
	aligned_file =  './processing/' + probe.split('/')[-1]   
        CropFace(image, eye_left=(left_y,left_x), eye_right=(right_y, right_x), offset_pct=(0.26,0.3), dest_sz=(120,140)).save(aligned_file)
	
	## extract features
	# lbp
	color = cv2.imread(aligned_file)
	image = cv2.cvtColor(color, cv2.COLOR_BGR2GRAY)
	lbp = local_binary_pattern( image, P= 8, R=2, method='nri_uniform')
	lbp = region_hist(lbp, 7, 6)
	
	# gabor
	#image = image[slice(0,None,4),slice(0,None,4)]
	#gabor = gabor_magnitude(image)
	#pca = joblib.load('./classifiers/gabor_pca.pkl')
	#gabor = pca.transform(gabor)

	# landmarks

	## classification
	lbp_clf = joblib.load('./classifiers/BU-CK_part_lbp_SVM.pkl')
	y_pred = lbp_clf.predict(lbp)
	lbp_prob = lbp_clf.predict_proba(lbp)

	#gabor_clf = joblib.load('./classifiers/BU-CK_gabor_SVM.pkl')
	#gabor_scores = gabor_clf.decision_function(gabor)
	#gabor_prob = gabor_clf.predict_proba(gabor)
	#lm_clf = joblib.load('./classifiers/CK_lm_svm.pkl')
	#lm_scores = lm_clf.decision_function(lm)
	#score = lbp_scores + gabor_scores + lm_scores
	#prob = lbp_prob + gabor_prob
	#y_pred = np.where(prob ==prob.argmax() )
	#score.flatten()
	#y_pred = lm_clf.predict(lm)
	
	return y_pred[0], lbp_prob